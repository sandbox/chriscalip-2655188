# Drupal Panels to Drupal Panels migration
## xmigrate_panels

## Description

This is a framework based on the Migrate API and migrate_d2d to ease building panel centric migrations
from one Drupal site to another.

Support at this time is only for :

* Drupal 7 as the only destination
* Drupal 6 & Drupal 7 as the only sources for initial release.

The core framework provided here is used by providing your own module, which
will register instances of the xmigrate_panels classes (or derivations of them).
See xmigrate_panels_example for one approach, where instances are registered when
the Drupal caches are cleared (note that registration updates previously-
registered classes with any argument changes).

This module supports migration of ctools_custom_content and needs it as a dependency.
