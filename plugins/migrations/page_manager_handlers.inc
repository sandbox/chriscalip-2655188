<?php

/**
 * @TODO
 */

/**
 * @TODO
 * AKA variants
 */
class XMigratePanelsHandlersMigration extends Migration {
  protected $sourceVersion = 6;
  protected $sourceConnection;
  protected $arguments;
  protected $tableSchema;
  protected $selectionRuleNodeTypeMigrationMaps;
  public function __construct($arguments) {
    parent::__construct($arguments);
    // aka Variants
    $table_name = 'page_manager_handlers';
    $this->arguments = $arguments;
    if ( !empty($arguments['source_version']) &&
      ( intval($arguments['source_version']) == 6 || intval($arguments['source_version']) == 7 )
      ) {
      $this->sourceVersion = intval($arguments['source_version']);
    }
    else {
      throw new MigrateException(t('No source_version provided in xmigrate_panels migration arguments.'));
    }
    if (!empty($arguments['source_connection'])) {
      $this->sourceConnection = $arguments['source_connection'];
    }
    else {
      throw new MigrateException(t('No source_connection provided in xmigrate_panels migration arguments.'));
    }
    // Requires pages migration as a dependency
    if (!empty($arguments['pages_migration'])) {
      $this->dependencies[] = $arguments['pages_migration'];
    }
    else {
      throw new MigrateException(t('No pages_migration provided in xmigrate_panels migration arguments.'));
    }
    // Requires layouts migration as a dependency
    if (!empty($arguments['layouts_migration'])) {
      $this->dependencies[] = $arguments['layouts_migration'];
    }
    else {
      throw new MigrateException(t('No layouts_migration provided in xmigrate_panels migration arguments.'));
    }
    // Populate Source
    $this->source = new MigrateSourceSQL($this->query(), array(), NULL, array('map_joinable' => FALSE));
    // Populate sensible defaults
    // Populate Destination
    $this->destination = new MigrateDestinationTable($table_name);
    // change this if d6 and d7 page_manager_pages structure diverges.
    $schema = drupal_get_schema($table_name);
    $schema_key = MigrateDestinationTable::getKeySchema($table_name);
    // source page_manager_pages field key [did]
    $source_key = $schema_key;
    // destination page_manager_pages field key [did]
    $destination_key = $schema_key;
    // Populate Migration Maps
    $this->map = new MigrateSQLMap($this->machineName, $source_key, $destination_key);
    $this->tableSchema = $schema;
    // Setup common mappings
    $this->addSimpleMappings(array(
      'name',
      'task',
      'subtask',
      'handler',
      'weight',
      'conf',
    ));
    // Handles Migration Binding of variants to its corresponding layout.
    if (!empty($arguments['layouts_migration'])) {
      $this->addFieldMapping('conf', 'conf', FALSE)->callbacks(array($this, 'remapVariantConf'));
    }
    $this->selectionRuleNodeTypeMigrationMaps = module_invoke_all('mpmm_selection_rules_node_type');
    // Handle (page_manager_pages) pid=>machine_name and its corresponding of migration record
  }
  /**
   * Query for page_manager_handlers records aka variants from Drupal 6 or Drupal 7.
   *
   * @return QueryConditionInterface
   */
  protected function query() {
    $table_name = 'page_manager_handlers';
    $query = Database::getConnection('default', $this->sourceConnection)
      ->select($table_name, 'v')
      ->fields('v');
    return $query;
  }
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    if (empty($this->tableSchema['fields'])) {
      return;
    }
    foreach ($this->tableSchema['fields'] as $field_name => $field) {
      if (isset($field['serialize']) &&
        $field['serialize'] &&
        !is_array($row->{$field_name})) {
        $row->{$field_name} = unserialize($row->{$field_name});
      }
    }
    // d6 -> d7 compatibility fix, d7 invalidates capitalize letters while d6 doesnt.
    if (!empty($row->name)) {
      $row->name = strtolower($row->name);
    }
  }
  /**
   * Enforces migration binding of page_managers_handlers to panels_display
   * Binding panels_display and page_manager_handlers.
   * Each of these entities have its own (did) primary key.
   * Each entity of page_manager_handlers is binded to panels_display through its conf column.
   * Inside a page variant's conf column is a foreign key (did) to panels_display.
   *
   * In essence replaces source conf['did'] to the migrated (did) value.
   *
   * @param array $conf
   *   incoming array $conf
   *
   * @return array
   *   The values array.
   */
  protected function remapVariantConf($conf) {
    if (empty($this->arguments['layouts_migration'])) {
      return array();
    }
    if (empty($conf)) {
      return array();
    }
    if (empty($conf) || !is_array($conf)) {
      return array();
    }
    if (empty($conf['did'])) {
      return array();
    }
    $source_layout_did = $conf['did'];
    // seems expensive to make use of Migration::getInstance($this->arguments['layouts_migration']);
    // and more code and
    // then $migrated_layout_did = $map->lookupDestinationID(array($source_layout_did));
    $prepend_table = 'migrate_map_';
    $table = $prepend_table . $this->arguments['layouts_migration'];
    if (db_table_exists($table) == FALSE) {
      return;
    }
    $migrated_layout_did = db_query("SELECT destid1 FROM {$table} WHERE sourceid1 = :sourceid1", array(
      ':sourceid1' => $source_layout_did))->fetchField();
    if (empty($migrated_layout_did)) {
      return;
    }
    $conf['did'] = $migrated_layout_did;
    $node_type_migration_maps = $this->selectionRuleNodeTypeMigrationMaps;
    // Implement Selection Rules Migration Mapping
    if ( !empty($conf['access']['plugins']) ) {
      foreach ($conf['access']['plugins'] as $row_number => $access_plugin) {
        // hook_mpmm_selection_rules_node_type migration map for selection rules node type.
        if ( !empty($node_type_migration_maps) &&
          ($access_plugin['name'] == 'node_type') &&
          !empty($access_plugin['settings'])) {
          foreach ($access_plugin['settings']['type'] as $aps_key_bundle => $old_bundle) {
            if (!empty($this->selectionRuleNodeTypeMigrationMaps[$old_bundle])) {
              $new_bundle = $this->selectionRuleNodeTypeMigrationMaps[$old_bundle];
              $conf['access']['plugins'][$row_number]['settings']['type'][$new_bundle] = $new_bundle;
              unset($conf['access']['plugins'][$row_number]['settings']['type'][$old_bundle]);
            }
          }
        }
        // section to switch out Current path type is one of "node/1, articles, node/2"
        // to "articles node/100 node/200", simulating source nid 1 to target nid 100 etc.
        elseif ( !empty($this->arguments['node_migrations']) &&
          ($access_plugin['name'] == 'path_visibility') &&
          !empty($access_plugin['settings']['paths'])) {
          $paths = preg_split('/\s+/', $access_plugin['settings']['paths']);
          $migration_maps = $parsed_paths = $source_ids = array();
          foreach ($paths as $path) {
            if (substr($path, 0, 5) != 'node/') {
              $parsed_paths[$path] = $path;
              continue;
            }
            $source_ids[] = substr($path, 5);
          }
          if (!empty($source_ids)) {
            $migration_maps = xmigrate_panels_get_migration_maps($this->arguments['node_migrations'], $source_ids);
          }
          if (!empty($migration_maps)) {
            foreach ($migration_maps as $source_nid => $target_nid) {
              $parsed_paths['node/' . $source_nid] = 'node/' . $target_nid;
            }
            $conf['access']['plugins'][$row_number]['settings']['paths'] = implode(' ', array_values($parsed_paths));
          }
        }
      }
    }
    return $conf;
  }
}
