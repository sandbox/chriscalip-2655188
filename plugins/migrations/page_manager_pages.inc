<?php

/**
 * @file plugins/destinations/d7/page.inc
 * @TODO
 */

/**
 * @TODO
 * AKA custom pages
 */
class XMigratePanelsPagesMigration extends Migration {
  /**
   * While much of the version-specific work can be done in the leaf classes,
   * to share data and behavior among all classes for a given Drupal version
   * we use this helper object.
   *
   * @var sourceVersion
   */
  protected $sourceVersion = 6;
  protected $sourceConnection;
  /**
   * Arguments for the containing migration. Primarily of interest for
   * the source_connection.
   *
   * @var array
   */
  protected $arguments;
  /**
   * Utility to be used for checking which row properties should
   * be converted as arrays.
   * @var array
   */
  protected $tableSchema;
  public function __construct($arguments) {
    parent::__construct($arguments);
    $table_name = 'page_manager_pages';
    $this->arguments = $arguments;
    if ( !empty($arguments['source_version']) &&
      ( intval($arguments['source_version']) == 6 || intval($arguments['source_version']) == 7 )
      ) {
      $this->sourceVersion = intval($arguments['source_version']);
    }
    else {
      throw new MigrateException(t('No source_version provided in xmigrate_panels migration arguments.'));
    }
    if (!empty($arguments['source_connection'])) {
      $this->sourceConnection = $arguments['source_connection'];
    }
    else {
      throw new MigrateException(t('No source_connection provided in xmigrate_panels migration arguments.'));
    }
    // Populate Source
    $this->source = new MigrateSourceSQL($this->query(), array(), NULL, array('map_joinable' => FALSE));
    // Populate sensible defaults
    // Populate Destination
    $this->destination = new MigrateDestinationTable($table_name);
    // change this if d6 and d7 page_manager_pages structure diverges.
    $schema = drupal_get_schema($table_name);
    $schema_key = MigrateDestinationTable::getKeySchema($table_name);
    // source page_manager_pages field key [pid]
    $source_key = $schema_key;
    // destination page_manager_pages field key [pid]
    $destination_key = $schema_key;
    // Populate Migration Maps
    $this->map = new MigrateSQLMap($this->machineName, $source_key, $destination_key);
    $this->tableSchema = $schema;
    // Setup common mappings
    $this->addSimpleMappings(array(
      'name',
      'task',
      'admin_title',
      'admin_description',
      'path',
      'access',
      'menu',
      'arguments',
      'conf',
    ));
    // Populate Override Migration Maps
  }
  /**
   * Query for page_manager_pages records from Drupal 6 or Drupal 7.
   *
   * @return QueryConditionInterface
   */
  protected function query() {
    $table_name = 'page_manager_pages';
    $query = Database::getConnection('default', $this->sourceConnection)
      ->select($table_name, 'p')
      ->fields('p');
    return $query;
  }
  /**
   * Called after the query data is fetched - we'll use this to convert
   * serialized strings to array.
   *
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    if (empty($this->tableSchema['fields'])) {
      return;
    }
    foreach ($this->tableSchema['fields'] as $field_name => $field) {
      if (isset($field['serialize']) &&
        $field['serialize'] &&
        !is_array($row->{$field_name})) {
        $row->{$field_name} = unserialize($row->{$field_name});
      }
    }
    // d6 -> d7 compatibility fix, d7 invalidates capitalize letters while d6 doesnt.
    if (!empty($row->name)) {
      $row->name = strtolower($row->name);
    }
  }
}
