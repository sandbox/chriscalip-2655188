<?php

/**
 * @file plugins/destinations/d7/page.inc
 * @TODO
 */

/**
 * @TODO
 * AKA use of layouts per variant
 */
class XMigratePanelsDisplaysMigration extends Migration {
  protected $sourceVersion = 6;
  protected $sourceConnection;
  protected $arguments;
  protected $tableSchema;
  protected $layoutMigrationMaps;
  // protected $defaultDisplayObject;
  public function __construct($arguments) {
    parent::__construct($arguments);
    $table_name = 'panels_display';
    $this->arguments = $arguments;
    if ( !empty($arguments['source_version']) &&
      ( intval($arguments['source_version']) == 6 || intval($arguments['source_version']) == 7 )
      ) {
      $this->sourceVersion = intval($arguments['source_version']);
    }
    else {
      throw new MigrateException(t('No source_version provided in xmigrate_panels migration arguments.'));
    }
    if (!empty($arguments['source_connection'])) {
      $this->sourceConnection = $arguments['source_connection'];
    }
    else {
      throw new MigrateException(t('No source_connection provided in xmigrate_panels migration arguments.'));
    }
    // Populate Source
    $this->source = new MigrateSourceSQL($this->query(), array(), NULL, array('map_joinable' => FALSE));
    // Populate sensible defaults
    // Populate Destination
    $this->destination = new MigrateDestinationTable($table_name);
    // change this if d6 and d7 page_manager_pages structure diverges.
    $schema = drupal_get_schema($table_name);
    $schema_key = MigrateDestinationTable::getKeySchema($table_name);
    // source page_manager_pages field key [did]
    $source_key = $schema_key;
    // destination page_manager_pages field key [did]
    $destination_key = $schema_key;
    // Populate Migration Maps
    $this->map = new MigrateSQLMap($this->machineName, $source_key, $destination_key);
    $this->tableSchema = $schema;
    // Setup common mappings
    $this->addSimpleMappings(array(
      'layout',
      'layout_settings',
      'panel_settings',
      'cache',
      'title',
      'hide_title',
      'title_pane',
    ));
    // Populate Override Migration Maps
    $this->layoutMigrationMaps = module_invoke_all('mpmm_layouts');
    // ctools_include('export');
    // $this->defaultDisplayObject = ctools_export_new_object('panels_display', TRUE);
    // @TODO implement more rigorous tie-in on variants->pages
  }
  /**
   * Query for panels_display records aka layout use from Drupal 6 or Drupal 7.
   *
   * @return QueryConditionInterface
   */
  protected function query() {
    $table_name = 'panels_display';
    $query = Database::getConnection('default', $this->sourceConnection)
      ->select($table_name, 'd')
      ->fields('d');
    return $query;
  }
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    if (empty($this->tableSchema['fields'])) {
      return;
    }
    foreach ($this->tableSchema['fields'] as $field_name => $field) {
      if (isset($field['serialize']) &&
        $field['serialize'] &&
        !is_array($row->{$field_name})) {
        $row->{$field_name} = unserialize($row->{$field_name});
      }
    }
    // d6 -> d7 compatibility fix, d7 invalidates capitalize letters while d6 doesnt.
    if (!empty($row->layout)) {
      $row->layout = strtolower($row->layout);
    }
    // hook_mpmm_layouts intended layouts switch source layout name to intended layout.
    if (!empty($this->layoutMigrationMaps) &&
      !empty($this->layoutMigrationMaps[$row->layout]) &&
      ($this->sourceVersion == $this->layoutMigrationMaps[$row->layout]['source_version']) &&
      !empty($this->layoutMigrationMaps[$row->layout]['target_layout']) ) {
      $row->layout = $this->layoutMigrationMaps[$row->layout]['target_layout'];
      // @TODO implement more rigorous layout migration map.
    }
  }
}
