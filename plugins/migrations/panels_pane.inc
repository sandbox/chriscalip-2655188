<?php

/**
 * @file plugins/destinations/d7/page.inc
 * @TODO
 */

/**
 * @TODO
 * AKA use of content types (as in panels content type) per variant
 */
class XMigratePanelsPaneMigration extends Migration {
  protected $sourceVersion = 6;
  protected $sourceConnection;
  protected $arguments;
  protected $tableSchema;
  protected $layoutMigrationMaps;
  protected $source_panels_display;
  protected $paneTypeMigrationMaps;
  // protected $defaultDisplayObject;
  public function __construct($arguments) {
    parent::__construct($arguments);
    $table_name = 'panels_pane';
    $this->arguments = $arguments;
    if ( !empty($arguments['source_version']) &&
      ( intval($arguments['source_version']) == 6 || intval($arguments['source_version']) == 7 )
      ) {
      $this->sourceVersion = intval($arguments['source_version']);
    }
    else {
      throw new MigrateException(t('No source_version provided in xmigrate_panels migration arguments.'));
    }
    if (!empty($arguments['source_connection'])) {
      $this->sourceConnection = $arguments['source_connection'];
    }
    else {
      throw new MigrateException(t('No source_connection provided in xmigrate_panels migration arguments.'));
    }
    // Requires layouts migration as a dependency
    if (!empty($arguments['layouts_migration'])) {
      $this->dependencies[] = $arguments['layouts_migration'];
    }
    else {
      throw new MigrateException(t('No layouts_migration provided in xmigrate_panels migration arguments.'));
    }
    // Populate Source
    $this->source = new MigrateSourceSQL($this->query(), array(), NULL, array('map_joinable' => FALSE));
    // Populate sensible defaults
    // Populate Destination
    $this->destination = new MigrateDestinationTable($table_name);
    // change this if d6 and d7 page_manager_pages structure diverges.
    $schema = drupal_get_schema($table_name);
    $schema_key = MigrateDestinationTable::getKeySchema($table_name);
    // source page_manager_pages field key [pid]
    $source_key = $schema_key;
    // destination page_manager_pages field key [pid]
    $destination_key = $schema_key;
    // Populate Migration Maps
    $this->map = new MigrateSQLMap($this->machineName, $source_key, $destination_key);
    $this->tableSchema = $schema;
    // Setup common mappings
    $this->addSimpleMappings(array(
      'panel',
      'type',
      'subtype',
      'shown',
      'access',
      'configuration',
      'cache',
      'style',
      'css',
      'extras',
      'position',
      // note : did is additionally parsed/triangulated at prepareRow
      'did',
    ));
    // Populate Override Migration Maps
    $this->layoutMigrationMaps = module_invoke_all('mpmm_layouts');
    $this->source_panels_display = Database::getConnection('default', $arguments['source_connection'])
      ->select('panels_display', 'd')
      ->fields('d')
      ->execute()
      ->fetchAllAssoc('did');
    $this->paneTypeMigrationMaps = module_invoke_all('mpmm_pane_types');
  }
  /**
   * Query for panels_pane records from Drupal 6 or Drupal 7.
   *
   * @return QueryConditionInterface
   */
  protected function query() {
    $table_name = 'panels_pane';
    $query = Database::getConnection('default', $this->sourceConnection)
      ->select($table_name, 'v')
      ->fields('v');
    return $query;
  }
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    if (empty($this->tableSchema['fields'])) {
      return;
    }
    foreach ($this->tableSchema['fields'] as $field_name => $field) {
      if (isset($field['serialize']) &&
        $field['serialize'] &&
        !is_array($row->{$field_name})) {
        $row->{$field_name} = unserialize($row->{$field_name});
      }
    }
    $prepend_table = 'migrate_map_';
    $table = $prepend_table . $this->arguments['layouts_migration'];
    if (db_table_exists($table) == FALSE) {
      return;
    }
    $migrated_layout_did = db_query("SELECT destid1 FROM {$table} WHERE sourceid1 = :sourceid1", array(
      ':sourceid1' => $row->did))->fetchField();
    if (empty($migrated_layout_did)) {
      return;
    }
    // hook_mpmm_layouts intended layouts, handle where panes go to which region.
    if (!empty($this->layoutMigrationMaps) &&
      !empty($this->source_panels_display)
      ) {
      $source_layout = $target_layout = '';
      if (!empty($this->source_panels_display[$row->did]->layout)) {
        $source_layout = $this->source_panels_display[$row->did]->layout;
        // map region
        if ( !empty($this->layoutMigrationMaps[$source_layout]['region_maps'][$row->panel]) ) {
          $row->panel = $this->layoutMigrationMaps[$source_layout]['region_maps'][$row->panel];
        }
      }
    }
    $row->did = $migrated_layout_did;
    // hook_mpmm_pane_types intended types & corresponding subtypes,.
    if (!empty($this->paneTypeMigrationMaps) &&
      !empty($this->paneTypeMigrationMaps[$row->type]) &&
      !empty($this->paneTypeMigrationMaps[$row->type]['target_type']) &&
      !empty($this->paneTypeMigrationMaps[$row->type]['subtype_maps']) &&
      !empty($this->paneTypeMigrationMaps[$row->type]['subtype_maps'][$row->subtype])
      ) {
      $migration_map = $this->paneTypeMigrationMaps[$row->type];
      // failsafe.
      if ( is_string($migration_map['target_type']) ) {
        $row->type = $migration_map['target_type'];
      }
      // failsafe
      if ( is_string($migration_map['subtype_maps'][$row->subtype]) ) {
        $row->subtype = $migration_map['subtype_maps'][$row->subtype];
      }
    }
  }
}
