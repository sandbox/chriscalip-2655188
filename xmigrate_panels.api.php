<?php

/**
 * @file
 * Documentation for hooks defined by XMigrate Panels.
 */

/**
 * Explains hook_mpmm_layouts().
 * Hook XMigrate Panels Migration Map for Layouts
 *
 * This example gives migration maps for two source layouts twocol & twocol_stacked.
 * twocol layout migrates to the target layout twocol_bricks
 * with the regions twocol:left going to twocol_bricks:left_above.
 *
 * Similarly source twocol_stacked migrating to
 * target layout onecol . All "panel_panes" in the 4 regions twocol_stacked
 * are migrated to onecol's middle.
 *
 * @return
 *   A multidimensional associative array with each element
 *   is the source layout machine name that has associative array
 *   with the following keys.
 *
 *   - source_version: Drupal Version (6|7).
 *   - target_version: Drupal Version (7) supported.
 *   - target_layout: migrate the source layout to this target layout; machine name
 *     of the target layout.
 *   - region_maps : associative array index by source region with a value of target region
 *
 */
function hook_mpmm_layouts() {
  $layout_migration_maps = array();
  $layout_migration_maps['twocol'] = array(
    'source_version' => 6,
    'target_version' => 7,
    'target_layout' => 'twocol_bricks',
    // source region maps to target region
    'region_maps' => array(
      // source => target
      'left' => 'left_above',
      'right' => 'right_below',
    ),
  );
  $layout_migration_maps['twocol_stacked'] = array(
    'source_version' => 6,
    'target_version' => 7,
    'target_layout' => 'onecol',
    'region_maps' => array(
      'top' => 'middle',
      'left' => 'middle',
      'right' => 'middle',
      'bottom' => 'middle',
    ),
  );
  return $layout_migration_maps;
}

/**
 * Explains hook_mpmm_pane_types().
 * Hook XMigrate Panels Migration Map for Pane Types and its substypes
 *
 * This example gives migration maps for two panel types :
 * node_body and content field.
 *
 * a.) node_body type migrates to the target type entity_field
 * with its subtype node_body to node:body. This handle drupal 6 and
 * drupal 7 version of how panels handles nody body.
 *
 * b.) content_field type migrates to the target entity_field; and its
 * subtypes field_lead_in maps to node:field_lead_in. This handles
 * the drupal 6 to drupal 7 version differences. The mapping
 * of field_oldfield_text to node:field_newfield_text handles
 * situation where d6 field_oldfield_text is renamed to field_newfield_text.
 *
 * @return
 *   A multidimensional associative array with each element
 *   is the source layout machine name that has associative array
 *   with the following keys.
 *
 *   - source_version: Drupal Version (6|7).
 *   - target_version: Drupal Version (7) supported.
 *   - target_type: migrate the source type to this target type;
 *   - region_maps : associative array index by source subtype with a value of target subtype
 *
 */
function hook_mpmm_pane_types() {
  $pane_type_migration_maps = array();
  $pane_type_migration_maps['node_body'] = array(
    'source_version' => 6,
    'target_version' => 7,
    'target_type' => 'entity_field',
    'subtype_maps' => array(
      'node_body' => 'node:body',
    ),
  );
  $pane_type_migration_maps['content_field'] = array(
    'source_version' => 6,
    'target_version' => 7,
    'target_type' => 'entity_field',
    'subtype_maps' => array(
      'field_lead_in' => 'node:field_lead_in',
      'field_oldfield_text' => 'node:field_newfield_text',
    ),
  );
  return $pane_type_migration_maps;
}

/**
 * Explains hook_mpmm_access_settings_node_type().
 * Hook XMigrate Panels Migration Map for Selection Rules Node Type.
 *
 * This example gives migration maps for
 * old site node bundle "old_bundle" mapped to the new site node bundle "new_bundle"
 *
 * Both key and corresponding value must be the machine names of the bundles.
 *
 * @return
 *   An associative array, index by source node bundle value by target node bundle.
 */
function hook_mpmm_selection_rules_node_type() {
  $access_settings_node_type_migration_maps = array();
  $access_settings_node_type_migration_maps['old_bundle'] = 'new_bundle';
  return $access_settings_node_type_migration_maps;
}
