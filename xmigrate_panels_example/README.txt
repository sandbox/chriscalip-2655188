XMigrate Panels Example
======================

This is example code for a Drupal 6 to Drupal 7 panels (pages,layouts,variants,panes) migration.
This module makes use of the migrate & xmigrate_panels migration framework. For further details
on migrate api see https://drupal.org/node/1006982 .

On xmigrate_panels_example_migrate_api()
The 'source_connection' => 'legacy' represents a drupal 6 database containing
the tables : page_manager_pages, page_manger_handlers, panels_display and page_pane.

This module just requires those drupal 6 tables to be available and populated at legacy;
all other tables are not necessary.

Migration How-To
================================

@see link
Getting started with Migrate
https://www.drupal.org/node/1006982

How To Execute Migration (Shortcut Edition)
===================================

drush ms
drush mi mpe_custom_content
drush mi mpe_layout_use
drush mi mpe_pages
drush mi mpe_panes
drush mi mpe_variants

drush ms :
** List all migrations with current status.
drush mi [migration-name]
** Executes migration of [migration-name]

Migration Implementation Classes
================================

On xmigrate_panels_example_migrate_api()

  $mpe_pages_arguments = array(
    'mpe_pages' => array(
      'class_name' => 'MpePagesMigration',
      'description' => t('Migration of page_manager_pages'),
    ),
  );

You have the option to make use of the corresponding parent class (XMigratePanelsPagesMigration)
instead of another migration class (MpePagesMigration) as there are no additional migrate_maps
overrides in play.

Legend Shorthands
==================
mpe is shorthand for xmigrate_panels_examples,
Mpe is shorthand XMigratePanelsExample

Migration Mapping Showcases.
================================

a.) Custom Content Panes (d6 formats fid based) are mapped to (d7 formats machine name based).

** This is implemented through xmigrate_panels_example_migrate_api() and its $common_arguments['format_mappings'] while the base class XMigratePanelsCCPanesMigration prepareRow method executes
intended format mappings.


b.) Panels Display has custom layout migration maps through implementation of hook_mpmm_layouts() via xmigrate_panels_example_mpmm_layouts().

c.) Panels Pane has custom pane types migration maps through implementation of hook_mpmm_pane_types() via xmigrate_panels_example_mpmm_layouts().
