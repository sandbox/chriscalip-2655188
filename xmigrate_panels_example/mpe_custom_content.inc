<?php

/**
 * @file mpe_custom_content.inc
 * Implementation of MpeCCPanesMigration for migration of
 * d6 source pages to d7 ctools_custom_content panes .
 *
 * 'Mpe' is a shorthand for XMigratePanelsExample
 */

/**
 * Handling migration of d6 source (ctools_custom_content) panes to d7 (ctools_custom_content) panes.
 */
class MpeCCPanesMigration extends XMigratePanelsCCPanesMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);
  }
}
