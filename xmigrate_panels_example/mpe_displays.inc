<?php

/**
 * @file mpe_displays.inc
 * Implementation of MpeDisplaysMigration for migration of
 * d6 source panel layouts to d7 panel layouts.
 *
 * 'Mpe' is a shorthand for XMigratePanelsExample
 */

/**
 * Handling migration of d6 source (panels_display) panel layouts to d7 panel layouts.
 */
class MpeDisplaysMigration extends XMigratePanelsDisplaysMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);
  }
}
