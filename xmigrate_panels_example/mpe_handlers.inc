<?php

/**
 * @file mpe_handlers.inc
 * Implementation of MpeHandlersMigration for migration of
 * d6 source d6 variants to d7 variants.
 *
 * 'Mpe' is a shorthand for XMigratePanelsExample
 */

/**
 * Handling migration of d6 source (page_manager_handlers) variants to d7 variants.
 */
class MpeHandlersMigration extends XMigratePanelsHandlersMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);
  }
}
