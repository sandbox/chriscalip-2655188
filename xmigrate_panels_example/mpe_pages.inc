<?php

/**
 * @file mpe_pages.inc
 * Implementation of MpePagesMigration for migration of
 * d6 source pages to d7 pages.
 *
 * 'Mpe' is a shorthand for XMigratePanelsExample
 */

/**
 * Handling migration of d6 source (page_manager_pages) pages to d7 pages.
 */
class MpePagesMigration extends XMigratePanelsPagesMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);
  }
}
