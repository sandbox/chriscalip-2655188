<?php

/**
 * @file mpe_panes.inc
 * Implementation of MpePanesMigration for migration of
 * d6 source panel panes to d7 panel panes.
 *
 * 'Mpe' is a shorthand for XMigratePanelsExample
 */

/**
 * Handling migration of d6 source (panels_pane) panes to d7 panes.
 */
class MpePanesMigration extends XMigratePanelsPaneMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);
  }
}
