<?php
/**
 * @file xmigrate_panels_example.migrate.inc
 */

/**
 * This is example code for a Drupal 6 to Drupal 7 panels (pages,layouts,variants,panes) migration.
 *
 * @link https://drupal.org/node/1006982
 */

/**
 * Implements hook_migrate_api().
 */
function xmigrate_panels_example_migrate_api() {
  // legend, mpe is shorthand for xmigrate_panels_examples,
  // legend, Mpe is shorthand XMigratePanelsExample
  $api = array(
    'api' => 2,
    'groups' => array(
      'mpe_group' => array(
        'title' => t('Migrate Test Group'),
      ),
    ),
  );
  $common_arguments = array(
    // 'legacy' represents a drupal 6 database containing the tables :
    // page_manager_pages, page_manger_handlers, panels_display and page_pane.
    // only these 4 tables are required.
    'source_connection' => 'legacy',
    'source_version' => 6,
    'group_name' => 'mpe_group',
    'default_uid' => 1,
    // d6 to d7 format mappings indexed by source d6 fid value of target d7 format machine name
    'format_mappings' => array(
      '1' => 'filtered_html',
      '2' => 'full_html',
    ),
  );
  // page_manager_pages Pages Migration
  $mpe_pages_arguments = array(
    'mpe_pages' => array(
      // can use XMigratePanelsPagesMigration instead if no additional
      // migration_map overrides are in play.
      'class_name' => 'MpePagesMigration',
      'description' => t('Migration of page_manager_pages'),
    ),
  );
  $common_mpe_pages_arguments = $common_arguments;
  foreach ($mpe_pages_arguments as $migration_name => $arguments) {
    $arguments = array_merge_recursive($arguments, $common_mpe_pages_arguments);
    $api['migrations'][$migration_name] = $arguments;
  }
  // panels_display Layouts Migration
  $mpe_layout_use_arguments = array(
    'mpe_layout_use' => array(
      'class_name' => 'MpeDisplaysMigration',
      'description' => t('Migration of Layouts Use (panels_display)'),
    ),
  );
  $common_mpe_layout_use_arguments = $common_arguments;
  foreach ($mpe_layout_use_arguments as $migration_name => $arguments) {
    $arguments = array_merge_recursive($arguments, $common_mpe_layout_use_arguments);
    $api['migrations'][$migration_name] = $arguments;
  }
  // panels_pane Panes Migration (requires layouts_migration)
  $mpe_pane_arguments = array(
    'mpe_panes' => array(
      'class_name' => 'MpePanesMigration',
      'description' => t('Migration of Panes (panels_pane)'),
      'layouts_migration' => 'mpe_layout_use',
    ),
  );
  $common_mpe_pane_arguments = $common_arguments;
  foreach ($mpe_pane_arguments as $migration_name => $arguments) {
    $arguments = array_merge_recursive($arguments, $common_mpe_pane_arguments);
    $api['migrations'][$migration_name] = $arguments;
  }
  // page_manager_handlers Variants Migrations
  // requires layouts_migration & pages_migration
  $mpe_variants_arguments = array(
    'mpe_variants' => array(
      'class_name' => 'MpeHandlersMigration',
      'description' => t('Migration of Variants (page_manager_handlers)'),
      'pages_migration' => 'mpe_pages',
      'layouts_migration' => 'mpe_layout_use',
    ),
  );
  $common_mpe_variants_arguments = $common_arguments;
  foreach ($mpe_variants_arguments as $migration_name => $arguments) {
    $arguments = array_merge_recursive($arguments, $common_mpe_variants_arguments);
    $api['migrations'][$migration_name] = $arguments;
  }
  // ctools_custom_content Panes Migration
  $mpe_custom_content_arguments = array(
    'mpe_custom_content' => array(
      // can use XMigratePanelsCCPanesMigration instead if no additional
      // migration_map overrides are in play.
      'class_name' => 'MpeCCPanesMigration',
      'description' => t('Migration of (ctools_custom_content) panes'),
    ),
  );
  $common_mpe_custom_content_arguments = $common_arguments;
  foreach ($mpe_custom_content_arguments as $migration_name => $arguments) {
    $arguments = array_merge_recursive($arguments, $common_mpe_custom_content_arguments);
    $api['migrations'][$migration_name] = $arguments;
  }
  return $api;
}
